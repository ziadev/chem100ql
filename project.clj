(defproject chem100ql "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [
                 [org.clojure/clojure "1.5.1"]
                 [org.apache.chemistry.opencmis/chemistry-opencmis-client-impl "0.8.0"]
                 [instaparse "1.2.4"]
                 [cmsql-grammar "0.1.3-SNAPSHOT"]
                 [chem100 "0.1.0-SNAPSHOT"]
                 [midje "1.5.1"]
                 ])
