(ns chem100ql.parsers.chem100ql
  (import org.apache.chemistry.opencmis.client.api.ItemIterable)
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (import org.apache.chemistry.opencmis.client.api.Document)
  (import org.apache.chemistry.opencmis.commons.enums.IncludeRelationships)
  (import java.lang.String)
  (:require [chem100.session :as session]
            [chem100.repository :as repo]
            [chem100.folder :as folder]
            [chem100.document :as doc]
            [chem100.query :as query]
            [chem100.cmisobject :as co]
            [chem100.typedefinition :as td]
            [cmsql-grammar.parsers.parseast :as parser]))

(use '[clojure.string :only (join split)])

(def session_map (atom {:default nil}))
(deref session_map)

(defn make-properties [columns values]
  (let [columns columns
        values values
        properties (new java.util.HashMap (apply assoc {} (interleave columns values)))
        ]
    ;(println "columns: " columns)
    ;(println "values: " values)
    ;(println "properties: " properties)
    properties))

(defn get-property [field result]
  (. (. result getPropertyByQueryName field) getFirstValue))

(defn convert->map [columns query-result]
  (loop [result {}
         columns columns]
    (if (not (seq columns)) result
      (let [first-column (first columns)]
        (recur (assoc result first-column (get-property first-column query-result)) (rest columns))))
    ))

(defn build-result-list [columns query-results]
    (loop [result ()
           prime query-results]
      (if (not (seq prime)) result
        (let [first (convert->map columns (first prime))]
          (recur (conj result first) (rest prime))))))

;; need a session, so we need a connection
(defn connect [statement]
  (let [parsed (:connect_query (parser/parse-tree statement))
        name (or (:connect_name parsed) "default")
        repo-param (session/create-parameter (:url parsed) (:user parsed) (:password parsed))
        session (session/create-session repo-param (repo/get-first-repository-id repo-param))]
    (swap! session_map assoc (keyword name) session)))

(defn build-select-query [columns type where-clause]
  (let [comma-delimited-columns (interpose "," columns)]
  (str "SELECT " (apply str comma-delimited-columns) " FROM " type " " where-clause)))

(defn create-select-context [session]
  (doto (session/create-operation-context session)
    (.setIncludeAcls false)
    (.setIncludeAllowableActions false)
    (.setIncludePolicies false)
    (.setIncludeRelationships IncludeRelationships/NONE)
    (.setMaxItemsPerPage 10000000) ;; default for now, probably not big enough
    (.setCacheEnabled false)))

;; add Limit... use .setMaxItemsPerPage
(defn exec-select [statement & [session-name]]
  (let [session-name (or session-name "default")
        session ((keyword session-name) @session_map)
        parsed (parser/parse-tree statement)
        type (:table-name parsed)
        columns (:column-names parsed)
        where-clause (str "WHERE " (join " " (flatten (:where parsed))))
        select-statement (build-select-query columns type where-clause)
        temp (println "temp:" select-statement)
        qr (query/query->seq session select-statement (create-select-context session))
        ;count (count qr)
        ]
    (build-result-list columns qr)
    ))

(connect "connect to http://localhost:8080/alfresco/cmisatom user admin password admin")
;(connect "connect to http://localhost:8080/alfresco/cmisatom user admin password admin AS alfresco")
;(parser/parse-tree "select * from cmis:folder")
;(parser/parse-tree "select * from cmis:folder where cmis:name like 'C%'")
;(exec-select "select cmis:name, cmis:objectId from cmis:folder")
(exec-select "select cmis:name, cmis:objectId from cmis:folder where cmis:name like 'C%'")
(exec-select "select cmis:name, cmis:objectId from cmis:folder where cmis:name = 'Company Home'")

(exec-select "select cmis:name, cmis:objectId from cmis:folder where cmis:name = 'Customers'")
(exec-select "select cmis:name, cmis:objectId, sched:daySchedule from sched:dayFolder where sched:scheduleYearMonthDay = '20140407'")
(exec-select "select cmis:name, cmis:objectId from sched:jobFolder where cmis:name like '%'")

