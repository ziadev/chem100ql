(ns chem100ql.prototype.ql
  (import org.apache.chemistry.opencmis.client.api.ItemIterable)
  (import org.apache.chemistry.opencmis.client.api.Folder)
  (import org.apache.chemistry.opencmis.client.api.Document)
  (import org.apache.chemistry.opencmis.commons.enums.IncludeRelationships)
  (import java.lang.String)
  (:require [chem100.session :as session]
            [chem100.repository :as repo]
            [chem100.folder :as folder]
            [chem100.document :as doc]
            [chem100.query :as query]
            [chem100.cmisobject :as co]
            [chem100.typedefinition :as td]
            [chem100.parsers.instaparse :as parser]))

(use '[clojure.string :only (join split)])

;; To Do
;; Chem100 Query Language
;; start with a fn (exec-???) and a string, later we can convert it to a macro and parameters; syntactic sugar
;; Add in select, delete, insert, update, connect macro's
;; should return a cmis:objectId, as needed
;; session context, as part of the calls, need keywords...
;; alfresco extensions, as part of the connection...
;; select cmis:objectId from path '/Editorial' -- no where clause, the path is the where clause

(def session_map (atom {:default nil}))

;; need a session, so we need a connection
(defn connect [statement & [name]]
  (let [name (or name "default")
        parsed (parser/parse-tree statement)
        repo-param (session/create-parameter (:url parsed) (:user parsed) (:password parsed))
        session (session/create-session repo-param (repo/get-first-repository-id repo-param))]
    (swap! session_map assoc (keyword name) session)))

(defn make-properties [columns values]
  (let [columns columns
        values values
        properties (new java.util.HashMap (apply assoc {} (interleave columns values)))
        ]
    ;(println "columns: " columns)
    ;(println "values: " values)
    ;(println "properties: " properties)
    properties))

;; INSERT stuff
;; do we return an object or cmis:objectId or is it an option?
;; get-children - need to look at code that gets the iterator from the response
(defn exec-insert [statement & [session-name]]
  (let [session-name (or session-name "default")
        session ((keyword session-name) @session_map)
        parsed (parser/parse-tree statement)
        insert-type (first (:insert-type parsed))
        path (first (:path parsed))
        object-id (:object-id parsed)
        columns (:columns parsed)
        values (:values parsed)
        properties (make-properties columns values)
        filename (. properties get "filename")
        mime-type (. properties get "mimeType")
        stream (if (not (= nil filename)) (session/create-content-stream session filename mime-type))
        test (if (not (= nil filename)) (. properties remove "filename"))
        test1 (if (not (= nil mime-type)) (. properties remove "mimeType"))
        ]
    (cond
     (= insert-type "folder") (folder/create-folder-with-props (co/get-object-by-path session path) properties)
     (= insert-type "document") (doc/create-document-with-props (co/get-object-by-path session path) properties)
     (= insert-type "file")  (doc/create-document-with-props (co/get-object-by-path session path) properties stream)
     (= insert-type "filex")  stream
     )))

;; SELECT stuff
(defn create-select-context [session]
  (doto (session/create-operation-context session)
    (.setIncludeAcls false)
    (.setIncludeAllowableActions false)
    (.setIncludePolicies false)
    (.setIncludeRelationships IncludeRelationships/NONE)
    (.setMaxItemsPerPage 10000000) ;; default for now, probably not big enough
    (.setCacheEnabled false)))

(defn build-select-query [columns type where-clause]
  (let [comma-delimited-columns (interpose "," columns)
        first-type (first type)]
  (str "SELECT " (apply str comma-delimited-columns) " FROM " first-type " " where-clause)))

(defn get-property [field result]
  (. (. result getPropertyByQueryName field) getFirstValue))

(defn convert->map [columns query-result]
  (loop [result {}
         columns columns]
    (if (not (seq columns)) result
      (let [first-column (first columns)]
        (recur (assoc result first-column (get-property first-column query-result)) (rest columns))))
    ))

(defn build-result-list [columns query-results]
    (loop [result ()
           prime query-results]
      (if (not (seq prime)) result
        (let [first (convert->map columns (first prime))]
          (recur (conj result first) (rest prime))))))

;; add Limit... use .setMaxItemsPerPage
(defn exec-select [statement & [session-name]]
  (let [session-name (or session-name "default")
        session ((keyword session-name) @session_map)
        parsed (parser/parse-tree statement)
        type (:table-name parsed)
        columns (:columns parsed)
        where-clause (join " "(:where-clause parsed))
        select-statement (build-select-query columns type where-clause)
        ;;temp (println "where-clause:" where-clause)
        qr (query/query->seq session select-statement (create-select-context session))
        ;;count (count qr)
        ]
     ;(cond
     ;(= insert-type "folder") (folder/create-folder-with-props (co/get-object-by-path (:default @session_map) path) properties)
     ;(= insert-type "document") (doc/create-document-with-props (co/get-object-by-path (:default @session_map) path) properties))
     ;(map test-fn1 qr)
    (build-result-list columns qr)
    ))

(defn exec-select-no-context [statement & [session-name]]
  (let [session-name (or session-name "default")
        session ((keyword session-name) @session_map)
        parsed (parser/parse-tree statement)
        type (:table-name parsed)
        columns (:columns parsed)
        where-clause (join " "(:where-clause parsed))
        select-statement (build-select-query columns type where-clause)
        ;;temp (println "where-clause:" where-clause)
        qr (query/query->seq session select-statement)
        ;;count (count qr)
        ]
     ;(cond
     ;(= insert-type "folder") (folder/create-folder-with-props (co/get-object-by-path (:default @session_map) path) properties)
     ;(= insert-type "document") (doc/create-document-with-props (co/get-object-by-path (:default @session_map) path) properties))
     ;(map test-fn1 qr)
    (build-result-list columns qr)
    ))

;; DELETE stuff
(defn create-delete-context [session]
  (doto (session/create-operation-context session)
    (.setIncludeAcls false)
    (.setIncludeAllowableActions false)
    (.setIncludePolicies false)
    (.setIncludeRelationships IncludeRelationships/NONE)
    (.setMaxItemsPerPage 10000000) ;; default for now, probably not big enough
    (.setCacheEnabled false)))

(defn exec-delete [statement & [session-name]]
  (let [session-name (or session-name "default")
        session ((keyword session-name) @session_map)
        parsed (parser/parse-tree statement)
        folder (:folder parsed)
        path? (:path? parsed)
        path (:path parsed)
        type (:table-name parsed)
        where-clause (join " " (:where-clause parsed))
        columns '("cmis:objectId")
        select-statement (build-select-query columns type where-clause)
        qr (if (= path? false) (query/query->seq session select-statement (create-delete-context session)))
        object-ids (if (= path? false) (flatten (map vals (build-result-list columns qr))))]
        (cond
         (and (= folder false)(= path? false)) (map (partial co/delete session) (seq (map (partial co/create-object-id session) object-ids)))
         (and (= folder true)(= path? false)) (map folder/delete-tree (seq (map (partial co/get-object session) object-ids)))
         (and (= folder true)(= path? true)) (folder/delete-tree (co/get-object-by-path session (create-delete-context session) (first path)))
         (and (= folder false)(= path? true)) (. (co/get-object-by-path session (create-delete-context session) (first path)) delete)
         )
    ))

;; UPDATE stuff
(defn update-properties [properties result]
  (. result updateProperties properties true))

(defn exec-update [statement & [session-name]]
  (let [session-name (or session-name "default")
        session ((keyword session-name) @session_map)
        parsed (parser/parse-tree statement)
        type (:table-name parsed)
        assignments (:assignments parsed)
        where-clause (join " " (:where-clause parsed))
        columns '("cmis:objectId")
        select-statement (build-select-query columns type where-clause)
        qr (query/query->seq session select-statement (create-select-context session))
        object-ids (flatten (map vals (build-result-list columns qr)))
        ]
    (map (partial update-properties assignments)
      (map (partial co/get-object session) object-ids))
    ))

;; type definitions
;(td/get-type-definition (:default @session_map) "D:dpp:person")
;(last (seq (td/get-type-children (:default @session_map) "cmis:folder")))

;; links/associations/relationships
